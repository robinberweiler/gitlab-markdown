

# About myself
## Basic facts about myself in short.

| Name | Age |Residence|Bachelor degree|
|-- |-- |--|--|
| Robin Berweiler | 27.11.1994 (27y) |Tiny room in Aachen or house in Seelscheid |Mechanical Engineer|

## In depth about my life
### Traveling 
I have traveled and moved quiet a lot. My two most notable trips been to Japan and to Brazil. Japan was a dream of mine which I would like to repeat some time in the future. I mean, just the food alone is reason enough to visit again. 🤤:joy:

![delicious food](https://www.tastingtable.com/img/gallery/20-japanese-dishes-you-need-to-try-at-least-once/l-intro-1644007893.jpg)

The reason why brazil seems special to me is, that I never thought that I will travel to brazil. But as my father moved to brazil, I have been traveled to brazil three times by now. Most beautiful beach in brazil I found so far is called [Pipa Beach](https://www.google.com/maps/place/Pipa+Beach/@-6.2263617,-35.0449123,357m/data=!3m1!1e3!4m5!3m4!1s0x7b28fe652749add:0x95163c192eb65218!8m2!3d-6.2268032!4d-35.045025), I really recommend to go there when you happen to make a trip to Brazil. :flag_br:

### Hobbies

I love Nature and technic alike. Therefore it's hard for me to define **the** favourate hobby that defines me. But to name the most important:

``` mermaid

graph TD;
    Hobbies-->Extroverted;
    Hobbies-->Introverted;
    Extroverted-->Diving;
    Extroverted-->Gaming;
    Extroverted-->Cooking;
   Introverted-->Cooking; 
   Introverted-->Gaming; 
   Introverted-->Music; 
   Introverted-->Tinkering/Learning; 



```
Following is a gif which I think describes my fascination and motivation for diving, gaming and kind of learning. This footage is from a game called [Subnatica Below Zero](https://store.steampowered.com/app/848450/Subnautica_Below_Zero/).


![Subnatica GIF](https://64.media.tumblr.com/1e3143fa7bbab328c7f8248545e4d227/56eb6e68663b4928-d3/s400x600/cafbdd1da77d0ad365a5ad23cd73271221a8c751.gifv)

Music is what keeps me going in general. I am always listening to music if I am able to. My spotify recap from last year shows this especially. :joy:  
Bottom left are the minutes of played music.

<img src="https://i.imgur.com/MB9Eb8Rh.jpg " width="300" height="500">


### A hobby which is more like a dream at the moment

Also there is a Hobby which I wish to push a little during this Master. I am not sure how I would call it, but being a GEEK would describe it the best I would say:


In short, I love to learn new things and impress people with stuff that I made. My goal is therefore to improve my skills in these "geeky" disciplines and make even cooler stuff in the future.


Some experimentations, which I will remove after the asignment is rated
------------------------------------------------

Everything in german are just some notes for me to understand what I learned there

## How to make Codeblocks: I hope this helps anyone:  
To create code blocks, indent every line of the block by at least four spaces or one tab.  
This is what a Codeblock looks like and  


    <html>
      <head>
      </head>
    </html>

If indenting is inconvenient, you could also fence the codeblock as following:  
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```






Absatz mit zwei leertasten am Ende der Zeile  
und dann gehts mit der nächsten Zeile weiter

"forced" Absatz mit zwei Absätzen

und dann gehts weiter...

_italic_ **bold**

[if you want to create a hyperlink](https://www.markdowntutorial.com/lesson/3/)

Um Referenz-links zu erstellen muss eine Referenz definiert werden mit Brackets und doppelpunkt [R]: Referenz.de  
Um eine Referenz zu nutzen wird [][] verwendet Beispiel [Link][R]

Bilder mit Referenz ![A pretty tiger][Bild]

[Bild]: https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg

file:///C:/Users/robin/Downloads/markmap.html

If you want to make an mindmap in markdown you can use the following code. (Thanks go to Orwa for having this code in his file, made the search really easy :smile:)

``` mermaid

graph TD;
    Big-->B;
    Big-->D;
    B-->D;
    C-->D;

```

Um mehr als einen Zeilenumbruch einzubauen nutze zb...

---

---

---

...*** oder _____ geht auch. 
