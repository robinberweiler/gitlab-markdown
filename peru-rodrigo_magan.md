## About Me

* **My Name:** Rodrigo Magan  
* **Age:** 22yr  
* **Nationality:** [Peru](https://th.bing.com/th/id/R.84bd12146d2551624baf7319ae648800?rik=b6cYrrakvq8l1g&riu=http%3a%2f%2fclipground.com%2fimages%2fperu-clipart-16.png&ehk=tRS0rUZ4qmMUzuTfK02JLWXHmnNGe31kV0EAvc%2f7FsI%3d&risl=&pid=ImgRaw&r=0)  
* **Bachelor:** Mechanical Engineering
<!--* **Studies:** Mechanical Engineering-->


## My hobbies...
* Play soccer :soccer:  
* Go out with friends :beers:  
* Read books :books:  
* Listening to music :arrow_forward:  

## Education
```mermaid
gantt
dateFormat  YYYY
title Timeline
excludes weekdays 2014

section School
Primary School in Peru         :done,    des1, 2005,2010
Secondary School in Peru       :done,    des2, 2010,2015
International Baccalaureate    :done,    des3, 2015,2016
section University
Bachelor Degree TU BS          :done,    des4, 2017,2022
Master Degree RWTH             :active,  des5, 2022,2024
```
  
---

### Tutorials
#### Bold and Italic
Writing in Markdown is _not_ that hard  
I **will** complete these lessons!  
"_Of course_," she whispered. Then, she shouted: "All I need is a **little moxie**!  
If you're thinking to yourself, **_This is unbelievable_**, you'd probably be right.  

#### Links
##### Inline links
The Latest News from [the BBC](www.bbc.com/news)

##### Reference links
Do you want to [see something fun][a fun place]?  
Well, do I have [the website for you][another fun place]!  

[a fun place]:www.zombo.com
[another fun place]:www.stumbleupon.com

#### Mermaid
```mermaid
graph TD;
A-->B;
A-->C;
B-->D;
C-->D;
```
