# MARKDOWN EXERCISE

_Writing in markdown is not that hard!_

**I will complete these lessons.**

_"Of course,"_ she whispered. Then, she shouted: **"blur blur blur"**.

**_bold & italics_**


![google](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Google_Homepage.svg/800px-Google_Homepage.svg.png)

To download the image above, just click [here](https://images.google.com/)


[Map][1]

[Earth][2]

[1]: https://www.google.de/maps?hl=en&tab=wl&authuser=0
[2]: https://earth.google.com/web/


I read this interesting quote the other day:

>"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!"
