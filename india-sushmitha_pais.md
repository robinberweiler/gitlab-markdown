## Headers 
# First degree header
## Second degree header
To make headers in Markdown, you preface the phrase with a hash mark (#). You place the same number of hash marks as the size of the header you want. For example, for a header one, you'd use one hash mark (# Header One), while for a header three, you'd use three (### Header Three)

# Markdown Notes & Tutorial Exercises

# To make a phrase italic in Markdown, you can surround words with an underscore (_)  
Make the word "not" italic  
Writing in Markdown is _not_ that hard!  

# To make phrases bold in Markdown, you can surround words with two asterisks ( ** )  
Make the word "will" bold  
I **will** complete these lessons!  

# Links - 2 Types
1. Inline link - To create an inline link, you wrap the link text in brackets ( [ ] ), and then you wrap the link in parenthesis ( ( ) ). For example, to create a hyperlink to www.github.com, with a link text that says, Visit GitHub!, you'd write this in Markdown: [Visit GitHub!](www.github.com)

2. Reference link - As the name implies, the link is actually a reference to another place in the document. 

Example: 
Link both of the words “hurricane” to the Wikipedia entry at https://w.wiki/qYn with a reference style link:  
< Hurricane Erika was the strongest and longest-lasting tropical cyclone in the 1997 Atlantic hurricane season. >  
[Hurricane][1] Erika was the strongest and longest-lasting tropical cyclone in the 1997 Atlantic [hurricane][1] season.

[1]:https://w.wiki/qYn

# Images using Markdown

## Example of how to add an image to the git - use ![](paste the image address link)
![Digitalization in the Construction Industry](https://www.douglascompany.com/wp-content/uploads/2018/12/The-Case-for-Construction-Technology.jpg)

# Using Reference links under images

![Manual Labour in construction Industry][Manual]

![Machines in construction Industr][Machine]

[Manual]: https://www.fresh50.com/wp-content/uploads/2019/10/images2547-5d9d2baf454c8-678x381.jpg

[Machine]: https://resources.news.e.abb.com/images/2021/5/19/0/Hero_Image_-_credit_Mesh_Mould_and_in_situ_Fabricator_ETH_Zurich_2016_2017_Gramazio_Kohler_Research_ETH_Zurich.jpg

# Blockquotes  
To create a block quote, all you have to do is preface a line with the "greater than" caret (>).  
Ex:  
>"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!"

# Lists
## Unordered Lists - To create an unordered list, you'll want to preface each item in the list with an asterisk ( * )  
* Bricks
* Cement
* Machinery
## Ordered lists - An ordered list is prefaced with numbers, instead of asterisks.
1. Bricks
2. Cement
3. Machinery
## Nested lists 
* Calculus
  * A professor
  * Has no hair
  * Often wears green
* Castafiore
  * An opera singer
  * Has white hair
  * Is possibly mentally unwell

1. Cut the cheese  
   Make sure that the cheese is cut into little triangles.
   
2. Slice the tomatoes
   Be careful when holding the knife.  
   For more help on tomato slicing, see Thomas Jefferson's seminal essay Tom Ate Those.

# Paragraphs
1. Explicit line breaks - Two spaces will put your sentence in the next line and this is called forced line break or soft break of a paragraph
2. Consecutive lines - Add a blank line to separate 2 paragraphs

# Kramdown Notes and Exercises
kramdown has two main classes of elements: 
1. **Block-level elements** are used to create paragraphs, headers, lists and so on
2. **Span-level elements** are used to markup text phrases as emphasized, as a link and so on.

# Example of Plantuml mindmap (Code Blocks) 
Code Blocks - kramdown supports two different code block styles. One uses lines indented with either four spaces or one tab whereas the other uses lines with tilde characters as delimiters – therefore the content does not need to be indented
~~~ use tilda's annd paste the special language that is supported by gitlab
~~~ plantuml
@startmindmap
* Debian
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio
@endmindmap
~~~ 

## Definition lists  
Definition lists are started when a normal paragraph is followed by a line starting with a colon and then the definition text  

term
: definition  

Another term
:  another definition 

## Tables 
* A line starting with a pipe character (|) starts a table row.
* However, if the pipe characters is immediately followed by a dash (-), a separator line is created
* If the pipe character is followed by an equal sign (=), the tables rows below it are part of the table footer

| Items | Store | Cost |  
|:------|:------|:------|
| Shoes | Decathlon | 150 Euro |
| Jacket | Zara | 300 Euro |
| Bag | H&M | 250 Euro |
| | | Total |
| | | 700 Euro |

## HTML Elements  
(div - division or a section, used as a container for HTML elements, p - paragraph element, pre - preformatted text, to be displayed as it is)
<div style="float: right">
Master's in Construction and Robotics course description
</div>

<div>
The main objective of the programme is the development and use of automated construction machinery and robotics as the basis for innovative construction processes on construction sites
</div>
<p>
[This is a link to the course website] (https://cr.rwth-aachen.de/)
</p>

