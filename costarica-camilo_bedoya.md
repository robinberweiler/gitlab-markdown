Camilo Bedoya :monkey:
-----------------------
### About me

I'm very passionate about how technology can transform the AEC industry and with that possitevly affect the enviromental issues we are facing right now. I'll like to really explore all the ambitious possibilites available in this course.

### Basics

- from [Costa Rica](https://www.google.com/search?q=costa+rica&rlz=1C5CHFA_enCR878CR878&sxsrf=APq-WBuNJXBU8b2QM47rl4b9tYEkdyODVg:1649880503566&source=lnms&tbm=isch&sa=X&sqi=2&ved=2ahUKEwjxydjq65H3AhV8xjgGHSouCY8Q_AUoAnoECAIQBA&biw=1365&bih=708&dpr=1)
- 24 years old
- Civil Engineer
- Latest Job: Head of Growth at [Kiwibot](https://www.kiwibot.com/)

<img src="https://scontent-frx5-1.xx.fbcdn.net/v/t1.6435-9/107002816_3388468194510636_1093309380953139933_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=zIlqlEusaRMAX8I8mAv&_nc_ht=scontent-frx5-1.xx&oh=00_AT9M_8a2a9zyYF5P-SAFLQcOlQUwAVTD9gvj0NHkANImBg&oe=627BCABD" alt="Camilo" width="200"/>

### HardSkills

- **Python** Beginner/ Intermediate 
- **Growth Marketing** Advanced
- **Excel/ Spreadsheets** Advanced
- **Power BI / Data Studio** Intermediate

### Soft Skills

- **Oral Presentation/ Pitching** Very Good
- **Management** Very Good
- **Interpersonal skills** Very Good 









### **After this is just random tries**





Markdown is not that hard!  
Let's try **bold moves**.  
Now some _italics_.  


First Level

>block quotes 
>>embedded block quotes

~~~ python
## Cool Python Code // In Spanish but still ok
def es_primo(number):
    for i in range(2,number - 1):
        if number % i == 0:
            return False
            break
    return True  


def run():
    numero = int(input('Ingrese un numero: '))
    if es_primo(numero) == True:
        print('Es un numero Primo')
    else:
        es_primo(numero) == False
        print('Es un numero no Primo')


if __name__ == '__main__':
    run()

~~~

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1200px-Python-logo-notext.svg.png)

1. Lists
    1. Embedded lists
2. Lists 

| Header1 | Header2 | Header3 |
|:--------|:-------:|--------:|
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |


| Table 1 |
|:--------|
| 10.20   |

This is a paragraph
{::comment}
This is a comment which is
completely ignored.
{:/comment}
... paragraph continues here.


{::comment}  
This text is completely ignored by kramdown - a comment in the text.  
{:/comment}  











This is a paragraph  
{::comment}  
This is a comment which is
completely ignored.  
{:/comment}
... paragraph continues here.  

Extensions can also be used
inline {::nomarkdown}**see**{:/}!


<p>This is a paragraph
<!-- 
This is a comment which is
completely ignored.
 -->
… paragraph continues here.</p>

<p>Extensions can also be used
inline **see**!</p>


A [link](https://cr.rwth-aachen.de/) to the CR Master's Website

This is an HTML
example.

*[HTML]: Hyper Text Markup Language


This is *red*{:style="color: red"}.   



