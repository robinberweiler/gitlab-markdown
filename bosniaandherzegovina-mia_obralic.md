# *First steps in the _Markdown_*
--------------------------------


## Personal information

My name is Mia and for is assignment, I will provide some information about me!:smiley:

I play piano and my favourite piano piece is 

[_"Clair de lune"_](https://www.youtube.com/watch?v=WNcsUNKlAKw). :musical_keyboard:


Besides that, I play guitar too and I like to improvise more than playing chords. :notes: :guitar:

It is really easy to learn chords for guitar and [_this website is really good for beginners start with_](https://www.ultimate-guitar.com/).


<img src="https://upload.wikimedia.org/wikipedia/commons/0/0a/Unplugged_%282009-02-15_by_irish10567%29.jpg" alt="https://upload.wikimedia.org/wikipedia/commons/0/0a/Unplugged_%282009-02-15_by_irish10567%29.jpg" width="500"/>


```diff
+I like camping and backpacking. 
```



<img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Angora-Peak-at-Sunset-Backpacking-Trip-_Oregon.jpg" width="500"/>

> Camping is nature's way of promoting the motel business. :tent:



```diff
- Some places where I have been backpacking:
```



* [Lost Coast](https://www.google.com/search?q=lost+coast&rlz=1C1CHBF_enUS989US990&sxsrf=APq-WBvCdfH_5ACAGbVhDJApClSIhtNDVQ:1650276172759&source=lnms&tbm=isch&sa=X&sqi=2&ved=2ahUKEwiM1rvorZ33AhXykZUCHWYpCt8Q_AUoAXoECAIQAw&biw=1536&bih=746&dpr=1.25#imgrc=x9aLnr5O7tQd0M)
* [Lake Tahoe](https://www.google.com/search?q=lake+tahoe&rlz=1C1CHBF_enUS989US990&sxsrf=APq-WBt6MQoS-3wbEzw2tP0g8Lvc0O4h-Q:1650276209984&source=lnms&tbm=isch&sa=X&sqi=2&ved=2ahUKEwi9wJv6rZ33AhXpSLgEHVXwDrAQ_AUoAXoECAIQAw&biw=1536&bih=746&dpr=1.25#imgrc=tfjIj8dPnVUmbM)
* [Monument Valley](https://www.google.com/search?q=monument+valley&rlz=1C1CHBF_enUS989US990&sxsrf=APq-WBtK_ISDnA8N2e88cFpUdUxCHyj5Rg:1650276247082&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjFmuqLrp33AhWH_6QKHfaVBPgQ_AUoAXoECAIQAw&biw=1536&bih=746&dpr=1.25#imgrc=Mq14FjWqMf1P7M)
* [Mountain Bjelasnica](https://www.google.com/search?q=bjelasnica&rlz=1C1CHBF_enUS989US990&sxsrf=APq-WBspllueZzp78HLkgSCwrQMLu_D3GQ:1650276278208&source=lnms&tbm=isch&sa=X&sqi=2&ved=2ahUKEwjY3d-arp33AhUTac0KHa8yCC4Q_AUoAXoECAIQAw&biw=1536&bih=746&dpr=1.25#imgrc=2CNIx9W7lyLTfM)



[![](https://mermaid.ink/img/pako:eNpVj7FuwzAMRH-F4Jz8gIcOjZOiU4dmkzvQEhMJtUWDlloEtv-9ctoANQECB97jATehFcdY4VVp8HCumwiwbpnDBJI8K3hp28Dj8ns_wH7_NL9FnqE2g5ck99_bx3_7_C0zHE2fx2BhUHHZpiBxy3jlEnIyyuRCvEIr8jlukJNkneHF2OKsBEUHLa1yi4WvEvRqniV3jvVh4w571p6CKwWnezMshXpusCrS8YVylxps4lLQPDhKfHQhiWJ1oW7kHVJO8n6LFqukmR9QHah07v-o5QcNGGux)](https://mermaid.live/edit#pako:eNpVj7FuwzAMRH-F4Jz8gIcOjZOiU4dmkzvQEhMJtUWDlloEtv-9ctoANQECB97jATehFcdY4VVp8HCumwiwbpnDBJI8K3hp28Dj8ns_wH7_NL9FnqE2g5ck99_bx3_7_C0zHE2fx2BhUHHZpiBxy3jlEnIyyuRCvEIr8jlukJNkneHF2OKsBEUHLa1yi4WvEvRqniV3jvVh4w571p6CKwWnezMshXpusCrS8YVylxps4lLQPDhKfHQhiWJ1oW7kHVJO8n6LFqukmR9QHah07v-o5QcNGGux)

_Markdown_ is really fun to use and I can't wait to learn more about it :computer: :smiley:
