
# Writing in Markdown is not that hard!    
## Writing in Markdown is not that hard!   
### Writing in Markdown is not that hard!   
#### Writing in Markdown is not that hard!

![markdown](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)  


My name is Bora Karadeniz. 

Here i start my first steps with **Markdown**.  

#### Table of Contents:  

 * [About Markdown](#About Markdown)  
 * [About me](#About me)  
    



# About Markdown  

With Markdown you can format texts. For example you can **bold**, _italicize_ or do _**both**_ with words. 

For the respective creator Markdown is **easy** to read and **easy** to write. 

![gif](https://mention.com/wp-content/uploads/2017/11/golf-google-alerts-mention.gif)


**Here a small overwiev of some commands**

----
 

![sheets](http://code.ahren.org/wp-content/uploads/Screenshot-17.png) 
<img src="http://code.ahren.org/wp-content/uploads/Screenshot-17.png" width="600" height="600">  



>_For more [Visit MarkdownGuide!](www.markdownguide.org)_


----



# About me

I am from Dortmund. I was born and raised there.  

You might know the football club :soccer: BVB :soccer:  

  

<img src="https://www.bvb.de/var/ezdemo_site/storage/images/media/bilder/galeriebilder/g_monaco_fans_choreo/1942096-1-ger-DE/G_Monaco_Fans_Choreo_bvbnachrichtenbild_regular.jpg" width="600" height="350">  

I also played in the youth.



![gif2](https://debeste.de/upload/306eb55ad5660410b3d64fa9699011ab2520.gif)  
  


Things are going similarly for Borussia Dortmund this year. :chart_with_downwards_trend: :arrow_lower_right:  :arrow_upper_right: :arrow_lower_right: :arrow_upper_right:

However, things are going better for my parents' hometown **Trabzon** in Turkey, where I have my roots.  

|          | club     | :soccer: | W | D | L | Goals | +/- | Pts |  
| ---      | ---      | ---      | --- | --- | --- | --- | --- | --- |
|    1     | Trabzonspor :trophy: | 33 | 21 | 10 | 2 | 60:28 | 32 | 73 |
|    2     | Fenerbahce  | 33 | 18 | 8 | 7 | 58:35 | 23 | 62 |
|    3     |  Konyaspor  | 33 | 18 | 7 | 8 | 56:36 | 20 | 61 |
|    :     |      :      | : |  : | : | : |   :   |  : |  : |
















