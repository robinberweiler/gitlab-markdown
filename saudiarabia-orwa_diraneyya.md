

# This is a title


## This is a title

This is *what* I want to say.  
Indeed it is.

``` mermaid

graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;

```

This is _what_ I want to say.

Indeed it is.


~~~ plantuml

@startmindmap
* Debian
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio
@endmindmap


~~~


![](https://img.etimg.com/thumb/msid-69534695,width-650,imgsize-183501,,resizemode-4,quality-100/watermelons.jpg)
