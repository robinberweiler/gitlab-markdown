# This is me

<details><summary>:house:</summary>

## Personal

</details>

<details><summary>:mortar_board:</summary>

## Education

</details>

<details><summary>:soccer: :books: :guitar:</summary>


## Freetime activities 

```plantuml
@startmindmap
+[#MediumPurple] Freetime Activities
++[#Salmon] indoor
+++[#pink] loud
++++[#MistyRose] eguitar
++++[#MistyRose] drums 
++++[#MistyRose] piano
+++[#pink] quiet
++++[#MistyRose] theology books 
++++[#MistyRose] home workouts

--[#DeepSkyBlue] outdoor
---[#LightSkyBlue] summer 
----[#APPLICATION] helping at church camp 
----[#APPLICATION] sport
-----[#Azure] soccer
-----[#Azure] volleyball
-----[#Azure] spikeball
---[#LightSkyBlue] winter
----[#APPLICATION] skiing / snowboarding
----[#APPLICATION] ice-skating
@endmindmap

```



</details>













